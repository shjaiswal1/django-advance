from django.urls import path
from messeges.views import index, show_messages

urlpatterns = [
    
    path('', index, name='index'),
    path('show_messages/', show_messages, name='show_messages'),
]